<?php

class WorkbenchOwner {
  private $transition;

  /** @return WorkbenchOwnerTransition */
  public function getTransition() {
    if (!isset($this->transition)) {
      $this->transition = new WorkbenchOwnerTransition();
    }
    return $this->transition;
  }

  /** @return WorkbenchOwnerFormAlterFactory */
  public function getFormAlterFor($form, $formState) {
    $formAlterFactory = new WorkbenchOwnerFormAlterFactory($this->getTransition());
    return $formAlterFactory->determineFormHandlerFor($form, $formState);
  }

  /** @return WorkbenchOwnerPermissionHandler */
  public function getPermissionHandlerFor($node, $user) {
    return new WorkbenchOwnerPermissionHandler($node, $user);
  }
}

abstract class WorkbenchOwnerBase {
  const OWNER_NOBODY = -1;
  const OWNER_FREE = -2;

  protected function determineValidUsersToState($state) {
    $rids = $this->determineValidRolesToState($state);
    return $this->getUsersHavingRoles($rids);
  }

  protected function determineValidRolesToState($state){
    $rids = array();
    $transitions = workbench_moderation_transitions();
    foreach ($transitions as $transition) {
      if ($transition->from_name == $state) {
        $permission = $this->getPermissionNameForTransition($transition->from_name, $transition->to_name);
        $roles = user_roles(TRUE, $permission);
        $rids = array_merge($rids, array_keys($roles));
      }
    }
    return array_unique($rids, SORT_REGULAR);
  }

  private function getPermissionNameForTransition($fromState, $toState) {
    return 'moderate content from ' . $fromState . ' to ' . $toState;
  }

  private function getUsersHavingRoles(array $rids) {
    $uids = array();
    $res = $this->queryUsersHavingRoles($rids);
    foreach ($res as $row) {
      $uids[] = $row->uid;
    }
    $users = array();
    $users = user_load_multiple($uids);
    return $users;
  }

  private function queryUsersHavingRoles(array $rids) {
    return db_select('users_roles', 'ur')
             ->fields('ur', array('uid'))
             ->condition('ur.rid', $rids, "IN")
             ->distinct()
             ->execute();
  }

  protected function getCurrentOwnerIdForNode($nid) {
    $res = $this->getQueryOwnerIdForNode($nid)
             ->execute()->fetchAll();
    return $this->returnOwnerFromResult($res);
  }

  protected function getLastOwnerIdForNodeBeforeHid($nid, $hid) {
    $res = $this->getQueryOwnerIdForNode($nid)
             ->condition('w.hid', $hid, '<')
             ->execute()->fetchAll();
    return $this->returnOwnerFromResult($res);
  }

  private function getQueryOwnerIdForNode($nid) {
    return db_select('workbench_moderation_node_history', 'w')
             ->condition('w.nid', $nid)
             ->fields('w', array('oid'))
             ->orderBy('w.hid', 'DESC')
             ->range(0, 1);
  }

  private function returnOwnerFromResult($res) {
    if (isset($res[0]->oid)) {
      return $res[0]->oid;
    }
    throw new Exception('Owner ID is unknown!');
  }
}

class WorkbenchOwnerTransition extends WorkbenchOwnerBase {
  private $node;
  private $receivedOwnerId;
  private $calculatedOwnerId;
  private $oldState;
  private $newState;

  /** @return WorkbenchOwner */
  public function transit($oldState, $newState) {
    $this->oldState = $oldState;
    $this->newState = $newState;
    if ($this->calculatedOwnerCanBeAssignedTo($newState)) {
      db_update('workbench_moderation_node_history')
        ->condition('nid', $this->getNodeNid())
        ->condition('hid', $this->getNodeHid())
        ->fields(array('oid' => $this->getCalculatedOwnerId()))
        ->execute();
    }
    return $this;
  }

  private function calculatedOwnerCanBeAssignedTo($state) {
    try {
      $ownerId = $this->getCalculatedOwnerId();
      if ($this->isSpecialOwner($ownerId)) {
        return TRUE;
      }
      $user = $this->getUserFor($ownerId);
      $rolesList = $this->determineValidRolesToState($state);
      $userRoles = array_keys($user->roles);
      foreach($rolesList as $key => $value) {
        if (in_array($value, $userRoles)) {
          return TRUE;
        }
      }
    }
    catch (Exception $e) {
    }
    return FALSE;
  }

  private function isSpecialOwner($ownerId) {
    return self::OWNER_NOBODY == $ownerId ||
           self::OWNER_FREE == $ownerId;
  }

  private function getUserFor($ownerId) {
    $user = user_load($ownerId);
    if (FALSE === $user) {
      throw new Exception('User cannot be loaded!');
    }
    return $user;
  }

  private function getCalculatedOwnerId() {
    if (!isset($this->calculatedOwnerId)) {
      $this->calculatedOwnerId = $this->calculateOwnerId();
    }
    return $this->calculatedOwnerId;
  }

  private function calculateOwnerId() {
    try {
      return $this->getReceivedOwnerId();
    }
    catch (Exception $e) {
      return $this->determineCalculatedOwnerIdBasedOnTransition();
    }
  }

  private function determineCalculatedOwnerIdBasedOnTransition() {
    if ($this->oldState == $this->newState) {
      return $this->calculateOwnerForNoStateChange();
    }
    return self::OWNER_NOBODY; 
  }

  private function calculateOwnerForNoStateChange() {
    try {
      $nid = $this->getNodeNid();
      $currentHid = $this->getNodeHid();
      return $this->getLastOwnerIdForNodeBeforeHid($nid, $currentHid);
    }
    catch (Exception $e) {
      return self::OWNER_NOBODY;
    }
  }

  /** @return WorkbenchOwner */
  public function setOwnerId($ownerId) {
    $this->receivedOwnerId = $ownerId;
    return $this;
  }

  private function getReceivedOwnerId() {
    if (!isset($this->receivedOwnerId)) {
      throw new Exception('No owner ID received!');
    }
    return $this->receivedOwnerId;
  }

  /** @return WorkbenchOwner */
  public function setNode($node) {
    $this->node = $node;
    return $this;
  }

  private function getNodeNid() {
    $node = $this->getNode();
    if (isset($node->nid)) {
      return $node->nid;
    }
    throw new Exception('Node ID is not known!');
  }

  private function getNodeHid() {
    $node = $this->getNode();
    if (isset($node->workbench_moderation['current']->hid)) {
      return $node->workbench_moderation['current']->hid;
    }
    throw new Exception('Current node history ID is not known!');
  }

  private function getNode() {
    if (isset($this->node)) {
      return $this->node;
    }
    throw new Exception('Node is not set!');
  }
}

class WorkbenchOwnerFormAlterFactory extends WorkbenchOwnerBase {
  private $transition;

  public function __construct($transition) {
    $this->transition = $transition;
  }

  public function determineFormHandlerFor($form, $formState) {
    if (isset($form['revision_information']['workbench_moderation_state_new'])) {
      return new WorkbenchOwnerFormAlterNodeRevisionInformation($form, $formState, $this->transition);
    }
    elseif (isset($form['options']['workbench_moderation_state_new'])) {
      return new WorkbenchOwnerFormHandlerNodeOptions($form, $formState, $this->transition);
    }
    elseif (isset($form['state'])) {
      return new WorkbenchOwnerFormAlterModeration($form, $formState, $this->transition);
    }
    return new WorkbenchOwnerFormAlterPassive($form, $formState);
  }
}

abstract class WorkbenchOwnerFormAlter extends WorkbenchOwnerBase {
  protected $form;
  protected $formState;

  public function __construct($form, $formState) {
    $this->form = $form;
    $this->formState = $formState;
  }

  public function getForm() {
    return $this->form;
  }

  public function getFormState() {
    return $this->formState;
  }

  /** @return WorkbenchOwnerFormAlterFactory */
  public function alterFormIfPossible() {
    try {
      $this->alterForm();
    }
    catch (Exception $e) {
    }
    return $this;
  }

  protected abstract function alterForm();
}

abstract class WorkbenchOwnerFormAlterActive extends WorkbenchOwnerFormAlter {
  private $transition;

  public function __construct($form, $formState, $transition) {
    parent::__construct($form, $formState);
    $this->transition = $transition;
  }

  protected function alterForm() {
    if (!$this->hasAccessToForm()) {
      throw new Exception('Do not have access to the form!');
    }
    $this->addWidget(array($this->getWidgetName() => $this->getWidget()));
    $this->addAjaxCallbackToState(array('#ajax' => $this->getAjaxCallback()));
    $this->addSubmitHandler();
  }

  private function hasAccessToForm() {
    $accessDenied = isset($this->form['#access']) && FALSE === $this->form['#access'];
    return !$accessDenied;
  }

  protected abstract function addWidget($widget);
  protected abstract function addAjaxCallbackToState($callback);

  protected function addSubmitHandler() {
    array_unshift($this->form['#submit'], 'workbench_owner_submit_handler');
  }

  public function handleSubmit() {
    $this->transition->setOwnerId($this->formState['values'][$this->getWidgetName()]);
  }

  protected function getAjaxCallback() {
    return array(
      'callback' => 'workbench_owner_ajax_callback',
      'wrapper' => $this->getWrapperName(),
      'effect' => 'fade',
      'event' => 'change',
    );
  }

  public function getAjaxResponse() {
    $widget = $this->getWidget();
    $commands = array(
      ajax_command_replace('#' . $this->getWrapperName(), drupal_render($widget)),
    );
    return array('#type' => 'ajax', '#commands' => $commands);
  }

  protected function getWidget() {
    return array(
      '#type' => 'select',
      '#title' => t('Owner'),
      '#description' => t('Select the owner for the next state'),
      '#options' =>  array(),
      '#multiple' => FALSE,
      '#prefix' => '<div id="' . $this->getWrapperName() . '">',
      '#suffix' => '</div>',
    );
  }

  protected function getSelectableOwnerOptionsForState($state) {
    $selectableUsers = $this->determineValidUsersToState($state);
    $selectableOwnerOptions = $this->getSpecialOwnerOptions();
    foreach ($selectableUsers as $uid => $account) {
      $selectableOwnerOptions[$account->uid] = $account->name;
    }
    return $selectableOwnerOptions;
  }

  private function getSpecialOwnerOptions() {
    return array(
      self::OWNER_NOBODY => '- Nobody -',
      self::OWNER_FREE => '- Free -',
    );
  }

  protected function getCurrentOwnerOptionForNode($nid) {
    try {
      return $this->getCurrentOwnerIdForNode($nid);
    }
    catch (Exception $e) {
      return self::OWNER_NOBODY;
    }
  }

  protected function getWidgetName() {
    return 'workbench_owner';
  }

  protected function getWrapperName() {
    return 'workbench-owner';
  }
}

abstract class WorkbenchOwnerFormAlterNode extends WorkbenchOwnerFormAlterActive {
  protected function addWidget($widget) {
    $this->form[$this->getFormWrapper()] += $widget;
  }

  protected function addAjaxCallbackToState($callback) {
    $this->form[$this->getFormWrapper()]['workbench_moderation_state_new'] += $callback;
  }

  protected function getWidget() {
    return array_merge(parent::getWidget(), array(
        '#options' => $this->getSelectableOwnerOptionsForState($this->getState()),
        '#default_value' => $this->getCurrentOwnerOptionForNode($this->form['#node']->nid),
    ));
  }

  private function getState() {
    if (isset($this->formState['values']['workbench_moderation_state_new'])) {
      return $this->formState['values']['workbench_moderation_state_new'];
    }
    return $this->form[$this->getFormWrapper()]['workbench_moderation_state_new']['#default_value'];
  }

  protected abstract function getFormWrapper();
}

class WorkbenchOwnerFormAlterNodeRevisionInformation extends WorkbenchOwnerFormAlterNode {
  protected function getFormWrapper() {
    return 'revision_information';
  }
}

class WorkbenchOwnerFormHandlerNodeOptions extends WorkbenchOwnerFormAlterNode {
  protected function getFormWrapper() {
    return 'options';
  }
}

class WorkbenchOwnerFormAlterModeration extends WorkbenchOwnerFormAlterActive {
  protected function addWidget($widget) {
    $this->form += $widget;
    $this->form['submit']['#weight'] = 1;
  }

  protected function addAjaxCallbackToState($callback) {
    $this->form['state'] += $callback;
  }

  protected function getWidget() {
    $widget = array_merge(parent::getWidget(), array(
        '#options' => $this->getSelectableOwnerOptionsForState($this->getState()),
        '#default_value' => $this->getCurrentOwnerOptionForNode($this->form['node']['#value']->nid),
    ));
    unset($widget['#description']);
    return $widget;
  }

  private function getState() {
    if (isset($this->formState['values']['state'])) {
      return $this->formState['values']['state'];
    }
    return $this->form['state']['#default_value'];
  }
}

class WorkbenchOwnerFormAlterPassive extends WorkbenchOwnerFormAlter {
  public function alterForm() {
    throw new Exception('Form cannot be altered!');
  }
}

class WorkbenchOwnerPermissionHandler extends WorkbenchOwnerBase {
  private $node;
  private $user;
  private $owner;

  public function __construct($node, $user) {
    $this->node = $node;
    $this->user = $user;
  }

  public function hasAccess() {
    try {
      return $this->tryToDetermineAccess();
    }
    catch (Exception $e) {
      return FALSE;
    }
  }

  private function tryToDetermineAccess() {
    if (!$this->hasUserAccessByModerationState()) {
      return FALSE;
    }
    return $this->isOwnerFree() ||
           $this->isUserTheOwner() ||
           $this->isUserTheNodeAuthor();
  }

  private function hasUserAccessByModerationState() {
    $rids = $this->determineValidRolesToState($this->getModerationState());
    foreach ($rids as $rid) {
      if (in_array($rid, array_keys($this->user->roles))) {
        return TRUE;
      }
    }
    return FALSE;
  }

  private function isOwnerFree() {
    return $this->getOwner() == self::OWNER_FREE;
  }

  private function isUserTheOwner() {
    return $this->getUserId() == $this->getOwner();
  }

  private function isUserTheNodeAuthor() {
    return $this->getUserId() == $this->getNodeAuthor();
  }

  private function getOwner() {
    if (!isset($this->owner)) {
      $this->owner = $this->getCurrentOwnerIdForNode($this->getNodeNid());
    }
    return $this->owner;
  }

  private function getNodeNid() {
    $node = $this->getNode();
    if (isset($node->nid)) {
      return $node->nid;
    }
    throw new Exception('Node ID is unknown!');
  }

  private function getNodeAuthor() {
    $node = $this->getNode();
    if (isset($node->uid)) {
      return $node->uid;
    }
    throw new Exception('Node author is unknown!');
  }

  private function getModerationState() {
    $node = $this->getNode();
    if (isset($node->workbench_moderation['current']->state)) {
      return $node->workbench_moderation['current']->state;
    }
    throw new Exception('Moderation state is unknown!');
  }

  private function getNode() {
    if (isset($this->node)) {
      return $this->node;
    }
    throw new Exception('Node is not set!');
  }

  private function getUserId() {
    $user = $this->getUser();
    if (isset($user->uid)) {
      return $user->uid;
    }
    throw new Exception('User ID is not known!');
  }

  private function getUser() {
    if (isset($this->user)) {
      return $this->user;
    }
    throw new Exception('User is not known!');
  }
}
