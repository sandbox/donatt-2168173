<?php

class WorkbenchOwnerFactory {
  private static $workbenchOwner;

  private function __construct() {
  }
  private function __clone() {
  }
  private function __wakeup() {
  }

  /** @return WorkbenchOwner */
  public static function getInstance() {
    if (!isset(self::$workbenchOwner)) {
      self::$workbenchOwner = new WorkbenchOwner();
    }
    return self::$workbenchOwner;
  }
}
