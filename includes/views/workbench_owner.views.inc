<?php
/**
 * @file
 *  Content moderation views.
 */

/**
 * Implements hook_views_data().
 */
function workbench_owner_views_data() {
  $data = array();
  $data['workbench_moderation_node_history']['oid'] = array(
    'title' => t('Moderation Owner'),
    'help' => t('The Owner of the content for the current moderation state'),
    'field' => array(
      'title' => t('Owner user ID'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'label' => t('Owner user'),
    ),
  );
  return $data;
}
